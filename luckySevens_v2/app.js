var starting$;
var remaining$;
var max$ = 0;
var currentTurn = 0;
var richestTurn = 0;
var tally = 0

function doStuff(){
    getCash();
    checkCash();
    setCash();
}

var getCash = function (){
    starting$ = parseInt(prompt("How much money have you got to gamble? Dollars only. We don't take change here!"));
}

var checkCash = function(){
    if(isNaN(starting$)){
        alert("You must enter dollars. No change either!");
        getCash();
    } else {
        console.log(starting$);
        return starting$;
    }
}

var setCash = function (){
    document.getElementById("cashLeft").textContent = "$" + starting$ + ".00";
    remaining$ = starting$;
    return remaining$;
}

var rollEm = function(){
    var die1 = Math.floor(Math.random()*6) + 1;
    document.getElementById("die1").textContent = die1;
    var die2 = Math.floor(Math.random()*6) + 1;
    document.getElementById("die2").textContent = die2;
    tally = die1 + die2;
    return tally;
}

var payEm = function(tally){
    if(tally == 7){
        remaining$ += 4;
        return remaining$;
    } else {
        remaining$ -= 1;
        return remaining$;
    }
}

var scoreKeeper = function(){
    currentTurn++
    document.getElementById("cashLeft").textContent = "$" + remaining$ + ".00";
    if(remaining$ > max$){
        max$ = remaining$;
        richestTurn = currentTurn;
    }
    console.log(tally, remaining$, max$, richestTurn, currentTurn)
    document.getElementById("starting$").textContent = "$" + starting$ + ".00";
    document.getElementById("max$").textContent = "$" + max$ + ".00";
    document.getElementById("currentTurn").textContent = currentTurn;
    document.getElementById("richestTurn").textContent = richestTurn;
    
}

var reset = function(){
    document.getElementById("results").style.display = "none";
    document.getElementById("playBtn").textContent = "Roll 'Em";
    document.getElementById("playBtn").onclick = playGame;
    doStuff();
}

var gameCheck = function(){
    if (remaining$ === 0){
        alert("You Lose");
        document.getElementById("results").style.display = "block";
        document.getElementById("playBtn").textContent = "Play Again";
        document.getElementById("playBtn").onclick = reset;
    }
}

var playGame = function(){
    rollEm();
    payEm(tally);
    scoreKeeper();
    gameCheck();
}

