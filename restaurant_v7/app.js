var validateForm = function(){
    var nameField = document.getElementById("nameField").value;
    var emailField = document.getElementById("emailField").value;
    var phoneField = document.getElementById("phoneField").value;
    var reasonInq = document.getElementById("reasonInq").value;
    var addInfo = document.getElementById("addInfo").value;
    var mon = document.getElementById("mon").checked;
    var tue = document.getElementById("tue").checked;
    var wed = document.getElementById("wed").checked;
    var thur = document.getElementById("thur").checked;
    var fri = document.getElementById("fri").checked;
    
    if(nameField == ""){
        alert("Name is required.")
        return false;
    }
    if(!Boolean((nameField && emailField) || (nameField && phoneField))){
        alert("We need your contact information. Please provide either an email address or phone number.");
        return false;
    }
    if(reasonInq == "other" && addInfo == ""){
        alert("Please add a comment why you are contacting us.");
        return false;
    }
    if(!(mon || tue || wed || thur || fri)){
        alert("Please select at least one day for us to contact you.")
        return false;
    }
}